function notify(message, title, type) {
    if(!title) {
        title = '';
    }
    else {
        title += '<hr />';
    }
    if(!type) {
        type = 'notice';
    }
    $.growl({
        delayOnHover: true,
        duration: 2222,
        fixed: false,
        location: 'tc',
        size: 'medium',
        style: type,
        close: 'X',
        title: title,
        message: message
    });
}
$(document).ready(function() {
    var editor = new SimpleMDE({
        autoDownloadFontAwesome: false,
        autofocus: true,
        autosave: {
            enabled: true,
            delay: 10000, // 10S
            uniqueId: 'editor-autosave'
        },
        element: document.getElementById("editor-area"),
        forceSync: true,
        hideIcons: ["guide"],
        indentWithTabs: false,
        lineWrapping: false,
        previewRender: function(plainText) {
            var self = this;
            setTimeout(function() {
                Prism.highlightAll();
            }, 0);
            return self.parent.markdown(plainText); // Returns HTML from a custom parser
        },
        promptURLs: false,
        renderingConfig: {
            singleLineBreaks: false,
            codeSyntaxHighlighting: true
        },
        spellChecker: false,
        // status: ["autosave", "lines", "words", "cursor"],
        status: false,
        styleSelectedText: true,
        tabSize: 4,
        toolbar: [
            "bold", "italic", "heading", "|",
            "quote", "unordered-list", "ordered-list", "|",
            "code", "table", "link", "image", "|",
            "preview", "side-by-side", "fullscreen"/*, "|", {
                name: "save",
                action: function customFunction(editor){
                    var _value = editor.value(),
                        _html = editor.gui.sideBySide.innerHTML;
                    console.log(_value);
                    console.log(_html);
                },
                className: "fa fa-save",
                title: "保存"
            }*/
        ]
    });
    if(GLOBAL.BLOG_ITEM) {
        editor.value(GLOBAL.BLOG_ITEM.markdown);
    }
    else {
        editor.value("This text will appear in the editor  \n\
            * http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/  \n\
            * http://localhost:5000/edit  \n\
            * https://dataset.readthedocs.io/en/latest/quickstart.html  \n\
            * http://v3.bootcss.com/components/#navbar  \n\
            * view-source:http://v3.bootcss.com/examples/blog/blog.css \n\
            * view-source:http://v3.bootcss.com/examples/blog/ \n\
            * view-source:http://v3.bootcss.com/examples/sticky-footer-navbar/sticky-footer-navbar.css \n\
            * view-source:http://v3.bootcss.com/examples/sticky-footer-navbar/ \n\
            * ... \n\
        ");
    }
    /*
    if(!editor.isFullscreenActive()) {
        editor.toggleFullScreen();
    }
    if(!editor.isSideBySideActive()) {
        editor.toggleSideBySide();
    }
    */
    if(!editor.isPreviewActive()) {
        SimpleMDE.togglePreview(editor);
    }
    // doc: http://silviomoreto.github.io/bootstrap-select/
    $('#sel-category').selectpicker({
        style: 'btn-default',
        size: 4
    });
    // doc1: http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
    // doc2: https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
    var citynames = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '/static/citynames.json',
            filter: function(list) {
                return $.map(list, function(cityname) {
                    return {
                        name: cityname
                    };
                });
            }
        }
    });
    citynames.initialize();
    $('#txt-tags').tagsinput({
        tagClass: function(item) {
            console.log(item);
            return 'label label-default label-important';
        },
        typeaheadjs: {
            name: 'citynames',
            displayKey: 'name',
            valueKey: 'name',
            source: citynames.ttAdapter()
        },
        freeInput: true,
        trimValue: true,
        allowDuplicates: false,
        maxChars: 15,
        maxTags: 10
    });
    if(GLOBAL.BLOG_ITEM) {
        $('#txt-title').val(GLOBAL.BLOG_ITEM.title);
        $('#txt-sketch').val(GLOBAL.BLOG_ITEM.sketch);
        $('#sel-category').val(GLOBAL.BLOG_ITEM.category);
        $('#txt-tags').val(GLOBAL.BLOG_ITEM.tags);
    }
    $('#btn-save').on('click', function() {
        var _title = $('#txt-title').val(),
            _value = editor.value(),
            _sketch = $('#txt-sketch').val()
            _category = $('#sel-category').val();
        var _html = editor.markdown(_value);
        var _tags = $('#txt-tags').val();
        // TODO: 解析tags, 取id
        console.log(_value)
        console.log(_html)
        console.log(_title)
        console.log(_category)
        console.log(_tags)
        var _data = {
            'title': _title,
            'markdown': _value,
            'sketch': _sketch,
            'html': _html,
            'category': _category,
            'tags': _tags
        };
        if(GLOBAL.BLOG_ITEM) {
            _data['id'] = GLOBAL.BLOG_ITEM.id;
        }
        $.ajax({
            url: GLOBAL.SAVE_URL,
            type: 'POST',
            dataType: 'json',
            data: _data,
            success: function(data) {
                console.log(data);
                notify(data.msg);
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
});