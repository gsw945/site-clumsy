# -*- coding: utf-8 -*-
import dataset


class DBHelper(object):
    """docstring for DBHelper"""
    def __init__(self, uri):
        super(DBHelper, self).__init__()
        engine_kwargs = {
            'connect_args': {
                'check_same_thread': False
            }
        }
        self.db = dataset.connect(uri, engine_kwargs=engine_kwargs)
        # from ptpdb import set_trace; set_trace();
    
    def close(self):
        if isinstance(self.db, dataset.database.Database):
            self.db.engine.dispose()