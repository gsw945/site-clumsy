# -*- coding: utf-8 -*-
import dataset


class VisitRecord(object):
    """docstring for VisitRecord"""
    def __init__(self, uri):
        super(VisitRecord, self).__init__()
        # sqlite默认会进行多线程访问检查, 在程序结束时会报错, 故此处禁用此检查
        engine_kwargs = {
            'connect_args': {
                'check_same_thread': False
            }
        }
        self.db = dataset.connect(uri, engine_kwargs=engine_kwargs)
    
    def close(self):
        if isinstance(self.db, dataset.database.Database):
            self.db.engine.dispose()